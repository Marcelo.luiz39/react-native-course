import { View, Image, StyleSheet } from "react-native";

export function Logo() {
  return (
    <View style>
      <Image source={require("../../assets/logo.png")} style={styles.image} />
    </View>
  );
}

const styles = StyleSheet.create({
  logoArea: {
    backgroundColor: "#4cbe6c",
    alignSelf: "flex-start",
    padding: 8,
    paddingLeft: 16,
    paddingRight: 20,
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
    borderBottomRightRadius: 32,
    borderBottomLeftRadius: 8,
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
    marginTop: 25,
  },
  logo: {
    color: "#fff",
    fontSize: 18,
    fontWeight: "bold",
  },
  image: {
    marginTop: 30,
    marginBottom: 20,
    width: 160,
    height: 50,
  },
});
