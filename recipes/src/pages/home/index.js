import React, { useState } from "react";
import {
  Text,
  StyleSheet,
  SafeAreaView,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";

import { Ionicons } from "@expo/vector-icons";

import { Logo } from "../../components/logo";

export function Home() {
  const [inputSearch, setInputSearch] = useState("");

  function handleSearch() {
    alert(`Pesquisando por: ${inputSearch}`);

    setInputSearch("");
  }

  return (
    <SafeAreaView style={styles.container}>
      <Logo />

      <Text style={styles.title}>Encontre a receita</Text>
      <Text style={styles.title}>perfeita para você</Text>

      <View style={styles.form}>
        <TextInput
          style={styles.input}
          placeholder="Digite o nome da receita..."
          value={inputSearch}
          onChangeText={(text) => setInputSearch(text)}
        />
        <TouchableOpacity onPress={handleSearch}>
          <Ionicons name="search" size={28} color="#4cbe6c" />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f3f9ff",
    paddingTop: 60,
    paddingStart: 14,
    paddingEnd: 14,
  },
  title: {
    fontSize: 26,
    fontWeight: "bold",
    color: "#051541",
  },
  form: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#fff",
    width: "100%",
    marginBottom: 16,
    marginTop: 16,
    borderWidth: 1,
    borderColor: "#ececec",
    borderRadius: 8,
    paddingLeft: 8,
    paddingRight: 8,
  },
  input: {
    width: "90%",
    maxWidth: "90%",
    height: 54,
  },
});
