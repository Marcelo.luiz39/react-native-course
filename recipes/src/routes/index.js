import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { Home } from "../pages/home";
import { Favorites } from "../pages/favorites";

import { Ionicons } from "@expo/vector-icons";

const { Navigator, Screen } = createBottomTabNavigator();

export function AppRoutes() {
  return (
    <Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarActiveTintColor: "#121212",
        tabBarHideOnKeyboard: true,

        tabBarStyle: {
          backgroundColor: "#fff",
          borderTopWidth: 0,
        },
      }}
    >
      <Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({ size, focused }) => {
            return (
              <Ionicons
                name={focused ? "home" : "home-outline"}
                size={size}
                color={focused ? "blue" : "#ccc"}
              />
            );
          },
        }}
      />
      <Screen
        name="Favoritos"
        component={Favorites}
        options={{
          tabBarIcon: ({ size, focused }) => {
            return (
              <Ionicons
                name={focused ? "heart" : "heart-outline"}
                size={size}
                color={focused ? "#ff4141" : "#ccc"}
              />
            );
          },
        }}
      />
    </Navigator>
  );
}
